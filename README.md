# README #

### What is this repository for? ###

It's a subscription module, with one feature as creating a subscription. 
Input parameters:-  
 * Amount to Charge
 * How often to charge customer: DAILY, WEEKLY, MONTHLY
 * If it�s monthly, the date of the month - i.e. 20
 * The start and end date of the entire subscription, with a maximum duration of 3 months

There is no exception handling however input request has been handled for invalid data. And in such cases
it would return an empty response.

### How do I get set up? ###

Import this repo as maven project and update project.

### Configuration ###
No configuration is required, as it used in-memory.
Clone to local machine:
git clone https://asish_sahoo@bitbucket.org/asish_sahoo/subscription.git

### How to run tests ###
Test cases can be run as Junit test cases.