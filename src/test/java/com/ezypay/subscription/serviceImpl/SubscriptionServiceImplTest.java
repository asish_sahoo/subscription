package com.ezypay.subscription.serviceImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ezypay.subscription.domain.SubscriptionTypeDetails;
import com.ezypay.subscription.enums.DAY;
import com.ezypay.subscription.enums.SubscriptionType;
import com.ezypay.subscription.service.impl.SubscriptionServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubscriptionServiceImplTest {
	
	@Autowired
	SubscriptionServiceImpl subscriptionServiceImpl;

	@Test
	public void fetchInvoiceDates_Weekly() {
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		List<String> invoice_dates= new ArrayList<String>();
		cal_start.set(2018, 4, 22);
		cal_end.set(2018, 5, 22);
		subscriptionTypeDetails.setSubscriptionType(SubscriptionType.WEEKLY);
		subscriptionTypeDetails.setDay(DAY.TUESDAY);
		invoice_dates=subscriptionServiceImpl.fetchInvoiceDates(subscriptionTypeDetails, cal_start.getTime(),cal_end.getTime());
		assertNotNull(invoice_dates);
		assertEquals(invoice_dates.size(),5);
	}
	
	@Test
	public void fetchInvoiceDates_Monthly() {
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		
		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		List<String> invoice_dates= new ArrayList<String>();
		cal_start.set(2018, 4, 22);
		cal_end.set(2018, 7, 22);
		
		subscriptionTypeDetails.setSubscriptionType(SubscriptionType.MONTHLY);
		subscriptionTypeDetails.setSubscriptionDay(25);
		
		invoice_dates=subscriptionServiceImpl.fetchInvoiceDates(subscriptionTypeDetails, cal_start.getTime(),cal_end.getTime());
		assertNotNull(invoice_dates);
		assertEquals(invoice_dates.size(),3);
	}
	
	@Test
	public void fetchInvoiceDates_Monthly_AfterDate() {
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		
		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		List<String> invoice_dates= new ArrayList<String>();
		cal_start.set(2018, 4, 22);
		cal_end.set(2018, 7, 22);
		
		subscriptionTypeDetails.setSubscriptionType(SubscriptionType.MONTHLY);
		subscriptionTypeDetails.setSubscriptionDay(25);
		
		invoice_dates=subscriptionServiceImpl.fetchInvoiceDates(subscriptionTypeDetails, cal_start.getTime(),cal_end.getTime());
		assertNotNull(invoice_dates);
		assertEquals(invoice_dates.size(),3);
	}
	
	@Test
	public void fetchInvoiceDates_Monthly_BeforeDate() {
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		
		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		List<String> invoice_dates= new ArrayList<String>();
		cal_start.set(2018, 4, 22);
		cal_end.set(2018, 7, 22);
		
		subscriptionTypeDetails.setSubscriptionType(SubscriptionType.MONTHLY);
		subscriptionTypeDetails.setSubscriptionDay(18);
		
		invoice_dates=subscriptionServiceImpl.fetchInvoiceDates(subscriptionTypeDetails, cal_start.getTime(),cal_end.getTime());
		assertNotNull(invoice_dates);
		assertEquals(invoice_dates.size(),3);
	}
}
