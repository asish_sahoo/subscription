package com.ezypay.subscription.controller;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ezypay.subscription.domain.AmountDetails;
import com.ezypay.subscription.domain.SubscriptionTypeDetails;
import com.ezypay.subscription.enums.Currency;
import com.ezypay.subscription.enums.DAY;
import com.ezypay.subscription.enums.SubscriptionType;
import com.ezypay.subscription.request.SubscriptionRequest;
import com.ezypay.subscription.response.SubscriptionResponse;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubscriptionControllerTest {

	@Autowired
	SubscriptionController subscriptionController;

	@Test
	public void subscribe_monthly_test() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		AmountDetails amountDetails = new AmountDetails();
		amountDetails.setCurrency(Currency.AUD);
		amountDetails.setValue(130f);
		subscriptionRequest.setAmountDetails(amountDetails);

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.set(2018, 4, 22);
		cal_end.set(2018, 7, 22);
		subscriptionRequest.setStartDate(cal_start.getTime());
		subscriptionRequest.setEndDate(cal_end.getTime());

		subscriptionTypeDetails.setSubscriptionDay(18);
		subscriptionTypeDetails.setSubscriptionType(SubscriptionType.MONTHLY);
		subscriptionRequest.setSubscriptionTypeDetails(subscriptionTypeDetails);
		SubscriptionResponse subscriptionResponse = subscriptionController.subscribe(subscriptionRequest);

		ObjectMapper mapper = new ObjectMapper();
		String jsonString;
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscriptionResponse);
			System.out.println(jsonString);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertNotNull(subscriptionResponse);
		assertEquals(subscriptionResponse.getInvoiceDates().size(), 3);
	}
	
	@Test
	public void subscribe_weekly_test() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		AmountDetails amountDetails = new AmountDetails();
		amountDetails.setCurrency(Currency.AUD);
		amountDetails.setValue(20f);
		subscriptionRequest.setAmountDetails(amountDetails);

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.set(2018, 1, 1);
		cal_end.set(2018, 2	, 1);
		subscriptionRequest.setStartDate(cal_start.getTime());
		subscriptionRequest.setEndDate(cal_end.getTime());

		subscriptionTypeDetails.setDay(DAY.TUESDAY);
		subscriptionTypeDetails.setSubscriptionType(SubscriptionType.WEEKLY);
		subscriptionRequest.setSubscriptionTypeDetails(subscriptionTypeDetails);
		SubscriptionResponse subscriptionResponse = subscriptionController.subscribe(subscriptionRequest);

		ObjectMapper mapper = new ObjectMapper();
		String jsonString;
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscriptionResponse);
			System.out.println(jsonString);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertNotNull(subscriptionResponse);
		assertEquals(subscriptionResponse.getInvoiceDates().size(), 4);
	}

	@Test
	public void subscribe_weekly_missingData() {
		SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
		SubscriptionTypeDetails subscriptionTypeDetails = new SubscriptionTypeDetails();
		AmountDetails amountDetails = new AmountDetails();
		amountDetails.setCurrency(Currency.AUD);
		amountDetails.setValue(20f);
		subscriptionRequest.setAmountDetails(amountDetails);

		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		cal_start.set(2018, 1, 1);
		cal_end.set(2018, 2	, 1);
		subscriptionRequest.setStartDate(cal_start.getTime());
		subscriptionRequest.setEndDate(cal_end.getTime());

		subscriptionTypeDetails.setDay(DAY.TUESDAY);
		//subscriptionTypeDetails.setSubscriptionType(SubscriptionType.WEEKLY);
		subscriptionRequest.setSubscriptionTypeDetails(subscriptionTypeDetails);
		SubscriptionResponse subscriptionResponse = subscriptionController.subscribe(subscriptionRequest);

		ObjectMapper mapper = new ObjectMapper();
		String jsonString;
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscriptionResponse);
			System.out.println(jsonString);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertNull(subscriptionResponse.getInvoiceDates());
	}
}
