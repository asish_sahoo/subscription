package com.ezypay.subscription.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ezypay.subscription.domain.Subscription;

@Transactional
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
	
	
}
