package com.ezypay.subscription.response;

import java.util.List;

import com.ezypay.subscription.domain.AmountDetails;
import com.ezypay.subscription.enums.SubscriptionType;

public class SubscriptionResponse{
	private Long id;
	private AmountDetails amountDetails;
	private SubscriptionType subscriptionType;
	private List<String> invoiceDates;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AmountDetails getAmountDetails() {
		return amountDetails;
	}
	public void setAmountDetails(AmountDetails amountDetails) {
		this.amountDetails = amountDetails;
	}
	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public List<String> getInvoiceDates() {
		return invoiceDates;
	}
	public void setInvoiceDates(List<String> invoiceDates) {
		this.invoiceDates = invoiceDates;
	}
}
