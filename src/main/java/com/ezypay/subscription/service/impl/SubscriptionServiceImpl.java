package com.ezypay.subscription.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.ezypay.subscription.domain.Subscription;
import com.ezypay.subscription.domain.SubscriptionTypeDetails;
import com.ezypay.subscription.enums.SubscriptionType;
import com.ezypay.subscription.repository.SubscriptionRepository;
import com.ezypay.subscription.request.SubscriptionRequest;
import com.ezypay.subscription.response.SubscriptionResponse;
import com.ezypay.subscription.service.SubscriptionService;

@Service
@Repository
public class SubscriptionServiceImpl implements SubscriptionService {

	/*
	 * Max allowed subscription time is 3months.
	 */
	private static final int max_subscription_allowed_time = 3;

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	@Autowired
	private Subscription subscription;
	
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceImpl.class);

	@Override
	public SubscriptionResponse subscribe(SubscriptionRequest subscriptionRequest) {
		SubscriptionResponse subscriptionResponse = new SubscriptionResponse();
		/*
		 * Validating request on all possible cases to ensure request is correct.
		 */
		if (!isValidSubscriptionRequest(subscriptionRequest)) {
			logger.error("Invalid Request Found");
			return subscriptionResponse;
		}
		List<String> invoiceDates = fetchInvoiceDates(subscriptionRequest.getSubscriptionTypeDetails(),
				subscriptionRequest.getStartDate(), subscriptionRequest.getEndDate());
		subscription.setCurrency(subscriptionRequest.getAmountDetails().getCurrency().toString());
		subscription.setInvoiceDates(invoiceDates.toString());
		subscription
				.setSubscriptionType(subscriptionRequest.getSubscriptionTypeDetails().getSubscriptionType().toString());
		subscription.setValue(subscriptionRequest.getAmountDetails().getValue());
		subscription = subscriptionRepository.saveAndFlush(subscription);
		subscriptionResponse = buildResponse(subscription, subscriptionRequest, invoiceDates);
		return subscriptionResponse;
	}

	public List<String> fetchInvoiceDates(SubscriptionTypeDetails subscriptionTypeDetails, Date startDate,
			Date endDate) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		List<String> invoiceDates = new ArrayList<String>();
		Calendar cal_start = Calendar.getInstance();
		Calendar cal_end = Calendar.getInstance();
		Calendar cal_max = Calendar.getInstance();

		cal_start.setTime(startDate);
		cal_end.setTime(endDate);
		cal_max.setTime(startDate);
		cal_max.add(Calendar.MONTH, max_subscription_allowed_time);

		cal_start = findFirstDay(subscriptionTypeDetails, startDate);

		if (cal_start.after(cal_end)) {
			return invoiceDates;
		}
		switch (subscriptionTypeDetails.getSubscriptionType()) {
		case DAILY:
			while ((cal_start.equals(cal_end) || cal_start.before(cal_end))
					&& (cal_start.equals(cal_max) || cal_start.before(cal_max))) {
				invoiceDates.add(format.format(cal_start.getTime()));
				cal_start.add(Calendar.DAY_OF_MONTH, 1);
			}
			break;

		case WEEKLY:
			while ((cal_start.equals(cal_end) || cal_start.before(cal_end))
					&& (cal_start.equals(cal_max) || cal_start.before(cal_max))) {
				invoiceDates.add(format.format(cal_start.getTime()));
				cal_start.add(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
			}
			break;

		case MONTHLY:
			while ((cal_start.equals(cal_end) || cal_start.before(cal_end))
					&& (cal_start.equals(cal_max) || cal_start.before(cal_max))) {
				invoiceDates.add(format.format(cal_start.getTime()));
				cal_start.add(Calendar.MONTH, 1);
			}
			break;
		}
		return invoiceDates;
	}

	public SubscriptionResponse buildResponse(Subscription subscription, SubscriptionRequest subscriptionRequest,
			List<String> invoiceDates) {
		SubscriptionResponse SubscriptionResponse = new SubscriptionResponse();
		SubscriptionResponse.setAmountDetails(subscriptionRequest.getAmountDetails());
		SubscriptionResponse.setId(subscription.getId());
		SubscriptionResponse.setInvoiceDates(invoiceDates);
		SubscriptionResponse
				.setSubscriptionType(subscriptionRequest.getSubscriptionTypeDetails().getSubscriptionType());
		return SubscriptionResponse;
	}

	public Calendar findFirstDay(SubscriptionTypeDetails subscriptionTypeDetails, Date startDate) {
		Calendar cal_start = Calendar.getInstance();
		int subscribe_day, start_day = 0;
		cal_start.setTime(startDate);
		SubscriptionType subscriptionType = subscriptionTypeDetails.getSubscriptionType();
		switch (subscriptionType) {
		case WEEKLY:
			subscribe_day = subscriptionTypeDetails.getDay().getCode();
			start_day = cal_start.get(Calendar.DAY_OF_WEEK);
			if (subscribe_day >= start_day) {
				cal_start.add(Calendar.DAY_OF_MONTH, subscribe_day - start_day);
			} else {
				cal_start.add(Calendar.DAY_OF_MONTH, 7 - subscribe_day + 1);
			}
			break;
		case MONTHLY:
			subscribe_day = subscriptionTypeDetails.getSubscriptionDay();
			start_day = cal_start.get(Calendar.DAY_OF_MONTH);
			if (subscribe_day >= start_day) {
				cal_start.add(Calendar.DAY_OF_MONTH, subscribe_day - start_day);
			} else {
				cal_start.add((Calendar.MONTH), 1);
			}
			break;
		default:
			break;
		}
		return cal_start;
	}

	public boolean isValidSubscriptionRequest(SubscriptionRequest subscriptionRequest) {
		Boolean isvalid = true;
		if (subscriptionRequest.getEndDate() == null || subscriptionRequest.getStartDate() == null
				|| subscriptionRequest.getAmountDetails().getCurrency() == null
				|| subscriptionRequest.getAmountDetails().getValue() == null
				|| subscriptionRequest.getSubscriptionTypeDetails().getSubscriptionType() == null) {
			logger.error("Invalid Request Found: Input parameters need to be provided correctly");
			isvalid = false;
		}
		if (subscriptionRequest.getEndDate().before(subscriptionRequest.getStartDate())) {
			logger.error("Invalid Request Found: End data can't be before start date");
			isvalid = false;
		}
		if (subscriptionRequest.getSubscriptionTypeDetails().getSubscriptionType() == SubscriptionType.WEEKLY
				&& subscriptionRequest.getSubscriptionTypeDetails().getDay() == null) {
			logger.error("Invalid Request Found: Subscription Day needs to be filled for Weekly type");
			isvalid = false;
		}
		if (subscriptionRequest.getSubscriptionTypeDetails().getSubscriptionType() == SubscriptionType.MONTHLY
				&& subscriptionRequest.getSubscriptionTypeDetails().getSubscriptionDay() == null) {
			logger.error("Invalid Request Found: Subscription Day needs to be filled for Monthly type");
			isvalid = false;
		}
		return isvalid; 
	}
}
