package com.ezypay.subscription.service;

import com.ezypay.subscription.request.SubscriptionRequest;
import com.ezypay.subscription.response.SubscriptionResponse;

public interface SubscriptionService {
	public SubscriptionResponse subscribe(SubscriptionRequest subscriptionRequest );
}
