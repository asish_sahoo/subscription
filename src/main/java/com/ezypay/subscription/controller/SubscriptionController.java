package com.ezypay.subscription.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ezypay.subscription.request.SubscriptionRequest;
import com.ezypay.subscription.response.SubscriptionResponse;
import com.ezypay.subscription.service.SubscriptionService;

@Controller
@EnableAutoConfiguration
public class SubscriptionController {

	@Autowired
	SubscriptionService subscriptionService;

	@RequestMapping(value = "/subscription", method = RequestMethod.POST)
	@ResponseBody
	public SubscriptionResponse subscribe(@Valid @RequestParam SubscriptionRequest subscriptionRequest) {
		SubscriptionResponse subscriptionResponse = subscriptionService.subscribe(subscriptionRequest);
		return subscriptionResponse;
	}

}
