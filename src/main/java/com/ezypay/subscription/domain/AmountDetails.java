package com.ezypay.subscription.domain;

import javax.validation.constraints.NotNull;

import com.ezypay.subscription.enums.Currency;

public class AmountDetails {
	@NotNull
	private Float value;
	@NotNull
	private Currency currency;

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	
}
