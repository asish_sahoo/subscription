package com.ezypay.subscription.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.ezypay.subscription.enums.DAY;
import com.ezypay.subscription.enums.SubscriptionType;

public class SubscriptionTypeDetails {
	
    @NotNull
	private SubscriptionType subscriptionType;
    @Min(31)
    private Integer subscriptionDay;
	private DAY day;
	
	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public Integer getSubscriptionDay() {
		return subscriptionDay;
	}
	public void setSubscriptionDay(Integer subscriptionDay) {
		this.subscriptionDay = subscriptionDay;
	}
	public DAY getDay() {
		return day;
	}
	public void setDay(DAY day) {
		this.day = day;
	} 
	
	
}
