package com.ezypay.subscription.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="subscription")
public class Subscription {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Float value;
	private String currency;
	private String subscriptionType;
	private String invoiceDates;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Float getValue() {
		return value;
	}
	public void setValue(Float value) {
		this.value = value;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public String getInvoiceDates() {
		return invoiceDates;
	}
	public void setInvoiceDates(String invoiceDates) {
		this.invoiceDates = invoiceDates;
	}	
}
