package com.ezypay.subscription.enums;

public enum Currency {
	AUD,
	USD,
	INR;
}
