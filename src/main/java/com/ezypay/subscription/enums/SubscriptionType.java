package com.ezypay.subscription.enums;

public enum SubscriptionType {
	DAILY,
	WEEKLY,
	MONTHLY;		
}
