package com.ezypay.subscription.request;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.ezypay.subscription.domain.AmountDetails;
import com.ezypay.subscription.domain.SubscriptionTypeDetails;

public class SubscriptionRequest {
	
		private AmountDetails amountDetails;
		private SubscriptionTypeDetails subscriptionTypeDetails;
		@NotNull
		private Date startDate;
		@NotNull
		private Date endDate;
		
		public AmountDetails getAmountDetails() {
			return amountDetails;
		}
		public void setAmountDetails(AmountDetails amountDetails) {
			this.amountDetails = amountDetails;
		}
		public Date getStartDate() {
			return startDate;
		}
		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}
		public Date getEndDate() {
			return endDate;
		}
		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
		public SubscriptionTypeDetails getSubscriptionTypeDetails() {
			return subscriptionTypeDetails;
		}
		public void setSubscriptionTypeDetails(SubscriptionTypeDetails subscriptionTypeDetails) {
			this.subscriptionTypeDetails = subscriptionTypeDetails;
		}
}
